package com.company;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.text.NumberFormat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class Main {
    public static void main (String args[]) throws IOException
    {
        String path = "D:\\实验一\\src\\main\\java\\com\\company\\one.txt";
        int k = 0;
        do
        {
            Scanner sc = new Scanner(System.in);
        System.out.println("输入实现数字代表功能：");
            System.out.println("-c 输出某个英文文本文件中 26 字母出现的频率，由高到低排列，并显示字母出现的百分比，精确到小数点后面两位。");
            System.out.println("-f 输出单个文件中的前 N 个最常出现的英语单词。");
            System.out.println("-d 跳过常用单词输出单个文件中的前 N 个最常出现的英语单词。");
            System.out.println("-p 显示常用的短语；");
            String order = sc.next();
            if(order.equals("-c"))
            {
                Double sum = 0.0;
                SortWord sortWord = new SortWord();
                File f = new File(path);
                HashMap<Character,Long> map = sortWord.sortWord(f);
                System.out.println(map.get(','));
                //将map.entrySet()转换成list
                List<Map.Entry<Character, Long>> list = new ArrayList<Map.Entry<Character, Long>>(map.entrySet());
                Collections.sort(list, new Comparator<Map.Entry<Character, Long>>() {


                    //降序排序
                    @Override
                    public int compare(Map.Entry<Character, Long> o1, Map.Entry<Character, Long> o2) {
                        //return o1.getValue().compareTo(o2.getValue());
                        return o2.getValue().compareTo(o1.getValue());
                    }
                });
                for (Map.Entry<Character, Long> mapping : list) {
                    sum += mapping.getValue();
                }
                for (Map.Entry<Character, Long> mapping : list) {
                    Double l = mapping.getValue().doubleValue();
                    System.out.println("单词:"+mapping.getKey() + "    出现次数:" + l + "占的百分比: " + String.format("%.2f",l/sum*100)+"%") ;


                }
            }
            else if(order.equals("-f"))
            {
                File file=new File(path);
                if(!file.exists())
                {
                    System.out.println("文件不存在");
                    return;
                }
                Scanner scanner=new Scanner(file);
                //单词和数量映射表
                HashMap<String, Integer > hashMap=new HashMap<String,Integer>();
                System.out.println("文章-----------------------------------");
                while(scanner.hasNextLine())
                {
                    String line=scanner.nextLine();
                    System.out.println(line);
                    String[] lineWords=line.split("\\W+");

                    Set<String> wordSet=hashMap.keySet();
                    for(int i=0;i<lineWords.length;i++)
                    {
                        //如果已经有这个单词了，
                        if(wordSet.contains(lineWords[i]))
                        {
                            Integer number=hashMap.get(lineWords[i]);
                            number++;
                            hashMap.put(lineWords[i], number);
                        }
                        else
                        {
                            hashMap.put(lineWords[i], 1);
                        }
                    }

                }
                System.out.println("统计单词：------------------------------");
                List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(hashMap.entrySet());
                Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {


                    //降序排序
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o2.getValue().compareTo(o1.getValue());
                    }
                });

                for (Map.Entry<String, Integer> mapping : list) {
                    System.out.println("单词:"+mapping.getKey() + "    出现次数:" + mapping.getValue());
                }

                System.out.println("程序结束--------------------------------");
            }
            else if (order.equals("-d")){
                File file=new File(path);
                if(!file.exists())
                {
                    System.out.println("文件不存在");
                    return;
                }
                Scanner scanner=new Scanner(file);
                //单词和数量映射表
                HashMap<String, Integer > hashMap=new HashMap<String,Integer>();
                System.out.println("文章-----------------------------------");
                while(scanner.hasNextLine())
                {
                    String line=scanner.nextLine();
                    System.out.println(line);
                    String[] lineWords=line.split("\\W+");

                    Set<String> wordSet=hashMap.keySet();
                    for(int i=0;i<lineWords.length;i++)
                    {
                        //如果已经有这个单词了，
                        if(wordSet.contains(lineWords[i]))
                        {
                            Integer number=hashMap.get(lineWords[i]);
                            number++;
                            hashMap.put(lineWords[i], number);
                        }
                        else
                        {
                            hashMap.put(lineWords[i], 1);
                        }
                    }

                }
                System.out.println("统计单词：------------------------------");
                List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(hashMap.entrySet());
                Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {


                    //降序排序
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o2.getValue().compareTo(o1.getValue());
                    }
                });

                for (Map.Entry<String, Integer> mapping : list) {
                    if (!(mapping.getKey().equals("of") || mapping.getKey().equals("the") || mapping.getKey().equals("it") || mapping.getKey().equals("and") || mapping.getKey().equals("this")))
                        System.out.println("单词:"+mapping.getKey() + "    出现次数:" + mapping.getValue());
                }

                System.out.println("程序结束--------------------------------");
            }
            else if(order.equals("-p"))
            {
                File file=new File(path);
                if(!file.exists())
                {
                    System.out.println("文件不存在");
                    return;
                }
                Scanner scanner=new Scanner(file);
                //单词和数量映射表
                HashMap<String, Integer > hashMap=new HashMap<String,Integer>();
                System.out.println("文章-----------------------------------");
                while(scanner.hasNextLine())
                {
                    String line=scanner.nextLine();
                    System.out.println(line);
                    String[] lineWords=line.split(",");

                    Set<String> wordSet=hashMap.keySet();
                    for(int i=0;i<lineWords.length;i++)
                    {
                        //如果已经有这个单词了，
                        if(wordSet.contains(lineWords[i]))
                        {
                            Integer number=hashMap.get(lineWords[i]);
                            number++;
                            hashMap.put(lineWords[i], number);
                        }
                        else
                        {
                            hashMap.put(lineWords[i], 1);
                        }
                    }

                }
                System.out.println("统计词组：------------------------------");
                List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(hashMap.entrySet());
                Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {


                    //降序排序
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o2.getValue().compareTo(o1.getValue());
                    }
                });

                for (Map.Entry<String, Integer> mapping : list) {
                    System.out.println("词组:"+mapping.getKey() + "    出现次数:" + mapping.getValue());
                }

                System.out.println("程序结束--------------------------------");
            }
            else
            {
                System.out.println("输入错误！");
            }
            System.out.println("输入是否继续（1为继续，0为退出）：");
            k = sc.nextInt();
        }while(k == 1);
    }

    private static String formattedDecimalToPercentage(double decimal)
    {
        //获取格式化对象
        NumberFormat nt = NumberFormat.getPercentInstance();
        //设置百分数精确度2即保留两位小数
        nt.setMinimumFractionDigits(2);
        return nt.format(decimal);
    }





}