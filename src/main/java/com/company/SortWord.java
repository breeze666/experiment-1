package com.company;

import com.sun.xml.internal.fastinfoset.util.CharArray;

import java.io.*;
import java.util.*;

public class SortWord {
    public static void main(String[] args) {

    }

    public  HashMap<Character,Long> sortWord(File file){
        String all = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            StringBuilder stringBuilder = new StringBuilder();
            String str;
            while ((str = bufferedReader.readLine()) != null){
                stringBuilder.append(str);
            }
            all = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HashMap<Character,Long> counter = new HashMap<>();
        assert all != null;
        all = all.toLowerCase();
        char[] chars = all.toCharArray();
        int length = all.length();
        for(int i = 0; i < length; i++) {
            if (counter.get(chars[i]) == null) {
                counter.put(chars[i], 1L);
            } else {
                counter.put(chars[i], counter.get(chars[i]) + 1);
            }
        }
        return counter;
    }
}
