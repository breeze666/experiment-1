package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class SplitWord {
    public static void main(String[] args) throws FileNotFoundException {
        File file=new File("D:\\实验一\\src\\com\\company\\one.txt");
        if(!file.exists())
        {
            System.out.println("文件不存在");
            return;
        }
        Scanner scanner=new Scanner(file);
        //单词和数量映射表
        HashMap<String, Integer > hashMap=new HashMap<String,Integer>();
        System.out.println("文章-----------------------------------");
        while(scanner.hasNextLine())
        {
            String line=scanner.nextLine();
            System.out.println(line);
            String[] lineWords=line.split("\\W+");

            Set<String> wordSet=hashMap.keySet();
            for(int i=0;i<lineWords.length;i++)
            {
                //如果已经有这个单词了，
                if(wordSet.contains(lineWords[i]))
                {
                    Integer number=hashMap.get(lineWords[i]);
                    number++;
                    hashMap.put(lineWords[i], number);
                }
                else
                {
                    hashMap.put(lineWords[i], 1);
                }
            }

        }
        System.out.println("统计单词：------------------------------");
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(hashMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {


            //降序排序
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        for (Map.Entry<String, Integer> mapping : list) {
            System.out.println("单词:"+mapping.getKey() + "    出现次数:" + mapping.getValue());
        }

        System.out.println("程序结束--------------------------------");
    }


    }
